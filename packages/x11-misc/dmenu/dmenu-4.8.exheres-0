# Copyright 2008 Gaute Hope <eg@gaute.vetsj.com>
# Copyright 2010 Markus Rothe
# Copyright 2012 Paul Seidler
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A dynamic menu for X"
DESCRIPTION="
Dynamic menu is a generic menu for X, originally designed for dwm. It manages huge amounts (up to
10.000 and more) of user defined menu items efficiently.
"
HOMEPAGE="https://tools.suckless.org/${PN}/"
DOWNLOADS="https://dl.suckless.org/tools/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        x11-proto/xorgproto
    build+run:
        media-libs/fontconfig
        media-libs/freetype:2
        sys-libs/zlib
        x11-libs/libX11
        x11-libs/libXft
        x11-libs/libXinerama
        x11-libs/libXrender
"

src_prepare() {
    # Set the correct prefix paths
    edo sed -e "s:PREFIX = /usr/local:PREFIX = /usr/$(exhost --target):" \
            -e "s:MANPREFIX = \${PREFIX}/share/man:MANPREFIX = /usr/share/man:" \
            -e "s:X11INC = /usr/X11R6/include:X11INC = /usr/$(exhost --target)/include:" \
            -e "s:X11LIB = /usr/X11R6/lib:X11LIB = /usr/$(exhost --target)/lib:" \
            -e "s:CC = cc:CC = $(exhost --target)-cc:" -i config.mk

    # respect global CFLAGS/LDFLAGS
    edo sed -e "/^CFLAGS/{s|=|+=|;s|-Os||g}" -i config.mk
    edo sed -e "/^LDFLAGS/{s|=|+=|;s|-s||g}" -i config.mk
}

