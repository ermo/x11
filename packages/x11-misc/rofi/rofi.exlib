# Copyright 2015 Vadim Intelegator <intelegator@gmail.com>
# Distributed under the terms of the GNU General Public License v2

SCM_libnkutils_REPOSITORY="https://github.com/sardemff7/libnkutils"
SCM_libgwater_REPOSITORY="https://github.com/sardemff7/libgwater"

SCM_SECONDARY_REPOSITORIES="libnkutils libgwater"
SCM_EXTERNAL_REFS="subprojects/libnkutils:libnkutils subprojects/libgwater:libgwater"

require github [ force_git_clone=true user='davatorium' ]
require meson

SUMMARY="A window switcher, run dialog and dmenu replacement"
DESCRIPTION="
A popup window switcher roughly based on superswitcher, requiring only xlib and
pango. Rofi developed extra features, like a run-dialog, ssh-launcher and can act
as a drop-in dmenu replacement, making it a very versatile tool."

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
    build+run:
        dev-libs/glib:2[>=2.40]
        gnome-desktop/librsvg:2[>=2.0]
        x11-libs/cairo[X]
        x11-libs/libxcb
        x11-libs/libxkbcommon[X]
        x11-libs/pango
        x11-libs/startup-notification
        x11-utils/xcb-util
        x11-utils/xcb-util-wm
        x11-utils/xcb-util-xrm
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    test:
        dev-libs/check[>=0.11.0]
        x11-apps/xkeyboard-config
"

