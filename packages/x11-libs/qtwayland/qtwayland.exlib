# Copyright 2014 Jorge Aparicio
# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Wayland plugin for Qt"
HOMEPAGE="http://qt-project.org/wiki/${PN}"

LICENCES+="
    GPL-3
    HPND [[ note = [ wayland-protocol, wayland-txt-input-unstable ] ]]
    MIT  [[ note = [ wayland-{ivi-extension-protocol,xdg-shell-protocol} ] ]]
"
MYOPTIONS="
    examples
"

# It's a bit surprising that this is in 5.12.5 but not in 5.13.1. It was
# backported from 5.14 and 5.12.5 was released a bit after 5.13.1, apparently
# late enough to receive the backport. But it also landed in the 5.13 branch
# in the meantime.
if ever at_least 5.13.1 ; then
    MYOPTIONS+=" vulkan"
else
    MYOPTIONS+="
        texture-sharing [[ description = [ EXPERIMENTAL texture sharing for NVIDIA ] ]]
    "
fi

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libxkbcommon[>=0.2.0]
        x11-libs/qtbase:${SLOT}[>=${PV}][gui]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
"

if ever at_least 5.13.1 ; then
    DEPENDENCIES+="
        build:
            vulkan? ( sys-libs/vulkan-headers )
        build+run:
            sys-libs/wayland[>=1.8.0]
    "
else
    DEPENDENCIES+="
        build:
            texture-sharing? ( sys-libs/vulkan-headers )
        build+run:
            sys-libs/wayland[>=1.6.0]
    "
fi

qtwayland_src_configure() {
    local qmake_parts=() qmake_enables=()

    if option examples ; then
        qmake_parts+=( QT_BUILD_PARTS+=examples )
    else
        qmake_parts+=( QT_BUILD_PARTS-=examples )
    fi

    if ever at_least 5.13.1 ; then
        qmake_enables+=(
            $(qt_enable vulkan feature-wayland-vulkan-server-buffer)
        )
    else
        qmake_enables+=(
            $(qt_enable texture-sharing feature-wayland-client-texture-sharing-experimental)
            $(qt_enable texture-sharing feature-wayland-compositor-texture-sharing-experimental)
        )
    fi

    eqmake "${qmake_parts[@]}" -- "${qmake_enables[@]}"
}

