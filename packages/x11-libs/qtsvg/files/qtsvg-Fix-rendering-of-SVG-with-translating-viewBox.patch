Upstream: under review, https://codereview.qt-project.org/c/qt/qtsvg/+/280802

From d34d7e5b46d4c58af737c7c95d2a01a6d96b55c0 Mon Sep 17 00:00:00 2001
From: Eirik Aavitsland <eirik.aavitsland@qt.io>
Date: Fri, 8 Nov 2019 14:32:51 +0100
Subject: [PATCH] Fix rendering of SVG with translating viewBox

This is a modification of14fa4591eb34a35cf3d485fd901e3f1e2caa7770.

That change correctly implemented support for scaling and centering
viewBox. However, the viewBox scaling and x-min/y-min translation was
applied in the wrong order, so SVGs with a non-zero x-min/y-min
viewBox would be rendered with an offset.

The autotest reflected this error, and is also fixed here. (In user
space, the black rectangle's upper left corner (0,0) is in the center
of the viewBox and so should end up in the center of the produced
image, independently of the scaling. All other tested SVG renderers
place it there.)

This change fixes all the reported regression cases from the above
commit, while not re-breaking the original cases fixed by that commit.

Fixes: QTBUG-79333
Fixes: QTBUG-78200
Task-number: QTBUG-70256
Change-Id: I0d3394e3caf6ec7edf16a10992c2fbfdac7398e5
---
 src/svg/qsvgtinydocument.cpp                 | 8 ++++----
 tests/auto/qsvgrenderer/tst_qsvgrenderer.cpp | 3 +--
 2 files changed, 5 insertions(+), 6 deletions(-)

diff --git a/src/svg/qsvgtinydocument.cpp b/src/svg/qsvgtinydocument.cpp
index 3143ad2..7e35508 100644
--- a/src/svg/qsvgtinydocument.cpp
+++ b/src/svg/qsvgtinydocument.cpp
@@ -434,10 +434,6 @@ void QSvgTinyDocument::mapSourceToTarget(QPainter *p, const QRectF &targetRect,
             // but the entire document. This attempts to emulate the default values of the <preserveAspectRatio>
             // tag that's implicitly defined when <viewbox> is used.
 
-            // Apply the view box translation if specified.
-            p->translate(target.x() - source.x(),
-                         target.y() - source.y());
-
             // Scale the view box into the view port (target) by preserve the aspect ratio.
             QSizeF viewBoxSize = source.size();
             viewBoxSize.scale(target.width(), target.height(), Qt::KeepAspectRatio);
@@ -448,6 +444,10 @@ void QSvgTinyDocument::mapSourceToTarget(QPainter *p, const QRectF &targetRect,
 
             p->scale(viewBoxSize.width() / source.width(),
                      viewBoxSize.height() / source.height());
+
+            // Apply the view box translation if specified.
+            p->translate(target.x() - source.x(),
+                         target.y() - source.y());
         }
     }
 }
diff --git a/tests/auto/qsvgrenderer/tst_qsvgrenderer.cpp b/tests/auto/qsvgrenderer/tst_qsvgrenderer.cpp
index cf19213..eb3ecba 100644
--- a/tests/auto/qsvgrenderer/tst_qsvgrenderer.cpp
+++ b/tests/auto/qsvgrenderer/tst_qsvgrenderer.cpp
@@ -269,14 +269,13 @@ void tst_QSvgRenderer::testMapViewBoxToTarget()
     }
 
     { // Viewport and viewBox specified -> scale 500x500 square to 1000x750 while preserving aspect ratio gives 750x750
-      // however the box is centered at 375, 250
         data = "<svg width=\"1000\" height=\"750\" viewBox=\"-250 -250 500 500\"><g><rect x=\"0\" y=\"0\" width=\"500\" height=\"500\" /></g></svg>";
         QPicture picture;
         QPainter painter(&picture);
         QSvgRenderer rend(data);
         rend.render(&painter);
         painter.end();
-        QCOMPARE(picture.boundingRect(), QRect(375, 250, 750, 750));
+        QCOMPARE(picture.boundingRect(), QRect(500, 375, 750, 750));
     }
 }
 
-- 
2.24.0

